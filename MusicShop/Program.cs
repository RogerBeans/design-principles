﻿
using MusicShop.StarterCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicShop
{
    class Program
    {
        static void Main(string[] args)
        {


            #region 1.StarterCode ( Initial App ) 
            // 1.StarterCode section 


            Inventory inventory = new Inventory();
            inventory.InitializeInventory();

            GuitarSpec whatJohnLikes = new GuitarSpec(Builder.FENDER, "stratocoastor", StarterCode.Type.ACOUSTIC, Wood.ADIRONDACK, Wood.ADIRONDACK);

            List<Guitar> matchingGuitars = inventory.Search(whatJohnLikes);
            if (matchingGuitars.Any()) {
                foreach (var guitar in matchingGuitars) {
                    GuitarSpec guitarSpec = guitar.getSpec();
                    Console.WriteLine($"John, you might like this guitar: {ExtensionMethods.GetBuilderString(guitarSpec.Builder)} , {guitarSpec.Model}, {guitarSpec.Type}, {guitarSpec.BackWood}, {guitarSpec.TopWood}, at only ${guitar.Price}!");
                }
                
                
            }

            Console.ReadKey();
        }

#endregion
            

    }
}
