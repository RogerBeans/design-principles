﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicShop.StarterCode
{
    public class Inventory
    {
        private IList<Guitar> _guitars;

        public Inventory() {
            _guitars = new List<Guitar>();
        }

        // This method takes in all the properties required to create a new Guitar instance, creates one, and adds it to the inventory
        public void AddGuitar(string serialNumber, double price, 
                             GuitarSpec guitarSpec) {

             Guitar guitar = new Guitar(serialNumber, price, guitarSpec);
            _guitars.Add(guitar);
        }

        public Guitar GetGuitar(String serialNumber) {
            foreach (var guitar in _guitars) {
                if (guitar.SerialNumber.Equals(serialNumber)) {
                    return guitar;
                }
            }

            return null;
        }

        public List<Guitar> Search(GuitarSpec searchSpec) {

            List<Guitar> matchingGuitars = new List<Guitar>();

            foreach (var guitar in _guitars) {
                // Ignore serial number since that's unique 
                // ignore price since that's unique


                // All of the information we use in coomparing guitars is in GuitarSpec now, not the guitar class.
                GuitarSpec guitarSpec = guitar.getSpec();

                Builder builder = searchSpec.Builder;
                if (searchSpec.Builder != guitarSpec.Builder) continue;

                // the only property that we need to worry about case on is the model, since that's still a String
                string model = searchSpec.Model.ToLower();
                if ((model != null) && (!model.Equals("")) && (!model.Equals(guitarSpec.Model))) continue;


                if (searchSpec.Type != guitarSpec.Type) continue;

                if (searchSpec.BackWood != guitarSpec.BackWood) continue;

                if (searchSpec.TopWood != guitarSpec.TopWood) continue;

                matchingGuitars.Add(guitar);                
            }

            // even though we changed our classes a bit, this method still returns a list of guitars that match the client's specs.
            return matchingGuitars;
        }


        public void InitializeInventory()
        {

            GuitarSpec guitarSpec1 = new GuitarSpec(Builder.FENDER, "stratocoastor", Type.ACOUSTIC, Wood.ADIRONDACK, Wood.ADIRONDACK);
            GuitarSpec guitarSpec2 = new GuitarSpec(Builder.GIBSON, "telecaster", Type.ACOUSTIC, Wood.ALDER, Wood.ALDER);
            GuitarSpec guitarSpec3 = new GuitarSpec(Builder.MARTIN, "Offset", Type.ELECTRIC, Wood.CEDAR, Wood.CEDAR);
            GuitarSpec guitarSpec4 = new GuitarSpec(Builder.OLSON, "les paul", Type.ELECTRIC, Wood.MAHOGANY, Wood.MAHOGANY);
            GuitarSpec guitarSpec5 = new GuitarSpec(Builder.FENDER, "sg and flying V", Type.ELECTRIC, Wood.MAPLE, Wood.MAPLE);
            GuitarSpec guitarSpec6 = new GuitarSpec(Builder.PRS, "Offset", Type.ACOUSTIC, Wood.ALDER, Wood.ALDER);
            GuitarSpec guitarSpec7 = new GuitarSpec(Builder.RYAN, "stratocoastor", Type.ACOUSTIC, Wood.ALDER, Wood.ALDER);
            GuitarSpec guitarSpec8 = new GuitarSpec(Builder.MARTIN, "stratocoastor", Type.ELECTRIC, Wood.INDIAN_ROSEWOOD, Wood.INDIAN_ROSEWOOD);
            GuitarSpec guitarSpec9 = new GuitarSpec(Builder.FENDER, "stratocoastor", Type.ACOUSTIC, Wood.ADIRONDACK, Wood.ADIRONDACK);

            Guitar guitar1 = new Guitar("", 0, guitarSpec1);
            Guitar guitar2 = new Guitar("", 0, guitarSpec2);
            Guitar guitar3 = new Guitar("", 0, guitarSpec3);
            Guitar guitar4 = new Guitar("", 0, guitarSpec4);
            Guitar guitar5 = new Guitar("", 0, guitarSpec5);
            Guitar guitar6 = new Guitar("", 0, guitarSpec6);
            Guitar guitar7 = new Guitar("", 0, guitarSpec7);
            Guitar guitar8 = new Guitar("", 0, guitarSpec8);
            Guitar guitar9 = new Guitar("", 100, guitarSpec9);

            _guitars.Add(guitar1);
            _guitars.Add(guitar2);
            _guitars.Add(guitar3);
            _guitars.Add(guitar4);
            _guitars.Add(guitar5);
            _guitars.Add(guitar6);
            _guitars.Add(guitar7);
            _guitars.Add(guitar8);
            _guitars.Add(guitar9);

        }

    }
}
