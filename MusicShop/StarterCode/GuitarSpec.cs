﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicShop.StarterCode
{

    // So we encapsulated what varies in Guitar class to GuitarSpec implementation. This way we are preventing duplication of code in terms of 
    // what properties clients use for searching guitar vs. what the Guitar Object holds.
    public class GuitarSpec
    {
        public Builder Builder { get; }
        public string Model { get; set; }
        public Type Type { get; set; }
        public Wood BackWood { get; set; }
        public Wood TopWood { get; set; }


        public GuitarSpec(Builder builder, string model, StarterCode.Type type, Wood backWood, Wood topWood) {

            this.Builder = builder;
            this.Model = model;
            this.Type = type;
            this.BackWood = backWood;
            this.TopWood = topWood;

        }
    }
}
