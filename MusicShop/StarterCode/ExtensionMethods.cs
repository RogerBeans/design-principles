﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicShop.StarterCode
{

    public enum Type { 
        ACOUSTIC, ELECTRIC
    }

    public enum Builder { 
        FENDER, MARTIN, GIBSON, COLLINGS, OLSON, RYAN, PRS
    }

    public enum Wood { 
        INDIAN_ROSEWOOD, MAHOGANY, MAPLE, CEDAR, ALDER, SITKA, ADIRONDACK
    }
   public static class ExtensionMethods
    {
        public static string GetTypeString(this Type type) {
            switch (type) {
                case Type.ACOUSTIC:
                    return "acoustic";
                case Type.ELECTRIC:
                    return "electric";
                default:
                    return "";
            }          

        }

        public static string GetBuilderString(this Builder builder) {
            switch (builder) {
                case Builder.FENDER:
                    return "fender";
                case Builder.MARTIN:
                    return "martin";
                case Builder.GIBSON:
                    return "gibson";
                case Builder.COLLINGS:
                    return "collings";
                case Builder.OLSON:
                    return "olson";
                case Builder.RYAN:
                    return "ryan";
                case Builder.PRS:
                    return "prs";
                default:
                    return "any";
            
            }            
        }

        public static string GetWoodString(this Wood wood) {
            switch (wood) {
                case Wood.ADIRONDACK:
                    return "adirondack";
                case Wood.ALDER:
                    return "alder";
                case Wood.CEDAR:
                    return "cedar";
                case Wood.INDIAN_ROSEWOOD:
                    return "indian rosewood";
                case Wood.MAHOGANY:
                    return "mahogany";
                case Wood.MAPLE:
                    return "maple";
                case Wood.SITKA:
                    return "sitka";
                default:
                    return "";
            }
        }



    }
}
